package com.github.quantrresearch.sharepoint.online;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class UploadTableModel extends DefaultTableModel {

	String columnNames[] = new String[]{"No", "File", "Status", "Start Time", "End Time", "Url"};
	public ArrayList<String> files = new ArrayList<>();
	public ArrayList<String> status = new ArrayList<>();
	public ArrayList<Date> startTime = new ArrayList<>();
	public ArrayList<Date> endTime = new ArrayList<>();
	public ArrayList<String> urls = new ArrayList<>();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

	@Override
	public String getColumnName(int column) {
		if (columnNames == null) {
			return null;
		}
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {
		if (columnNames == null) {
			return 0;
		}
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		if (files == null) {
			return 0;
		}
		return files.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		try {
			if (column == 0) {
				return row + 1 + " ";
			} else if (column == 1) {
				return files.get(row);
			} else if (column == 2) {
				return status.get(row);
			} else if (column == 3) {
				return sdf.format(startTime.get(row));
			} else if (column == 4) {
				return sdf.format(endTime.get(row));
			} else if (column == 5) {
				return urls.get(row);
			} else {
				return null;
			}
		} catch (Exception ex) {
			return "";
		}
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
	}
}
