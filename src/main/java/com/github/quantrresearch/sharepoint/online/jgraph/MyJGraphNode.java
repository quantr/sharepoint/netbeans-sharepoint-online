//package com.github.quantrresearch.sharepoint.online.jgraph;
//
//import java.io.Serializable;
//import java.util.List;
//import java.util.Vector;
//
///**
// *
// * @author Peter <mcheung63@hotmail.com>
// */
//public class MyJGraphNode implements Serializable {
//
//	private String label;
//	private List<MyJGraphNode> children = new Vector<>();
//
//	public MyJGraphNode(String label) {
//		this.label = label;
//	}
//
//	public String getLabel() {
//		return label;
//	}
//
//	public void setLabel(String label) {
//		this.label = label;
//	}
//
//	public List<MyJGraphNode> getChildren() {
//		return children;
//	}
//}
