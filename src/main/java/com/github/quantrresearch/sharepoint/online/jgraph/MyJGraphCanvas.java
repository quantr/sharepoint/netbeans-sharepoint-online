package com.github.quantrresearch.sharepoint.online.jgraph;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.CellRendererPane;
import javax.swing.JLabel;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxInteractiveCanvas;
import com.mxgraph.view.mxCellState;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class MyJGraphCanvas extends mxInteractiveCanvas {

	CellRendererPane rendererPane = new CellRendererPane();

	JPanel panel = new JPanel();
	JLabel label = new JLabel();
	JLabel statusLabel = new JLabel();
	JLabel iconLabel = new JLabel();

	mxGraphComponent graphComponent;

	static Color borderColor = Color.black;
	static Color backgroundcolor = new Color(240, 240, 240);

	public MyJGraphCanvas(mxGraphComponent graphComponent) {
		this.graphComponent = graphComponent;

		label.setBorder(BorderFactory.createLineBorder(borderColor));
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setBackground(backgroundcolor);
		label.setOpaque(true);
		label.setForeground(Color.black);
		label.setFont(new Font("arial", Font.PLAIN, 14));

		iconLabel.setIcon(new ImageIcon(this.getClass().getResource("/com/github/quantrresearch/sharepoint/online/fileicon/excel.png")));
		iconLabel.setBorder(BorderFactory.createLineBorder(borderColor));
		iconLabel.setBackground(backgroundcolor);
		iconLabel.setOpaque(true);
		statusLabel.setForeground(new Color(0, 200, 0));

		panel.setBackground(backgroundcolor);
		panel.setOpaque(true);
		panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.CENTER);
		panel.add(statusLabel, BorderLayout.SOUTH);
		panel.add(iconLabel, BorderLayout.WEST);
	}

	public void drawVertex(mxCellState state, String str) {
		iconLabel.setText("<html><body style=\"padding:5px\">" + str + "<br><font color=green>100% processed</font></body></html>");
		rendererPane.paintComponent(g, iconLabel, graphComponent, (int) (state.getX() + translate.getX()), (int) (state.getY() + translate.getY()), (int) state.getWidth(), (int) state.getHeight(), true);
	}

}
