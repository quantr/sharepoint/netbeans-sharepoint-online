package com.github.quantrresearch.sharepoint.online.jgraph;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxInteractiveCanvas;
import com.mxgraph.view.mxGraph;

public class MyGraphComponent extends mxGraphComponent {

	public MyGraphComponent(mxGraph graph) {
		super(graph);
	}

	@Override
	public mxInteractiveCanvas createCanvas() {
		return new MyJGraphCanvas(this);
	}
}
