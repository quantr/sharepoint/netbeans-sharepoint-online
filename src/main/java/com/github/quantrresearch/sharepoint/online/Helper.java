// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package com.github.quantrresearch.sharepoint.online;

import java.util.ArrayList;

/**
 *
 * @author Peter <mcheung63@hotmail.com>
 */
public class Helper {

	public static Object getNode(SharePointTreeNode node, Class c) {
		if (node.object != null && node.object.getClass() == c) {
			return node;
		} else {
			if (node.getParent() == null) {
				return null;
			} else {
				return getNodeObject((SharePointTreeNode) node.getParent(), c);
			}
		}
	}

	public static Object getNodeObject(SharePointTreeNode node, Class c) {
		if (node.object != null && node.object.getClass() == c) {
			return node.object;
		} else {
			if (node.getParent() == null) {
				return null;
			} else {
				return getNodeObject((SharePointTreeNode) node.getParent(), c);
			}
		}
	}

	public static String escapeSharePointUrl(String path) {
		return path.replaceAll(" ", "%20");
	}

	public static ArrayList<SharePointTreeNode> getNodes(SharePointTreeNode node, String type) {
		ArrayList<SharePointTreeNode> nodes = new ArrayList<SharePointTreeNode>();
		getNodes(node, nodes, type);
		return nodes;
	}

	public static void getNodes(SharePointTreeNode node, ArrayList<SharePointTreeNode> nodes, String type) {
		//System.out.println(node + ", type=" + node.type);
		if (node.type.equals(type)) {
			nodes.add(node);
		}
		for (int x = 0; x < node.getChildCount(); x++) {
			SharePointTreeNode child = (SharePointTreeNode) node.getChildAt(x);
			getNodes(child, nodes, type);
		}
	}
}
