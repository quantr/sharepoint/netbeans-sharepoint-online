package com.github.quantrresearch.sharepoint.online;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ScanningTableModel extends DefaultTableModel {

	String columnNames[] = new String[]{"No", "File", "Formula", "Replacement"};
	public ArrayList<String> files = new ArrayList<>();
	public ArrayList<String> formulas = new ArrayList<>();
	public ArrayList<String> replacements = new ArrayList<>();

	@Override
	public String getColumnName(int column) {
		if (columnNames == null) {
			return null;
		}
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {
		if (columnNames == null) {
			return 0;
		}
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		if (files == null) {
			return 0;
		}
		return files.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		try {
			if (column == 0) {
				return row + 1 + " ";
			} else if (column == 1) {
				return files.get(row);
			} else if (column == 2) {
				return formulas.get(row);
			} else if (column == 3) {
				return replacements.get(row);
			} else {
				return null;
			}
		} catch (Exception ex) {
			return "";
		}
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
	}
}
