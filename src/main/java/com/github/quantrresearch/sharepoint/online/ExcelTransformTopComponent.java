package com.github.quantrresearch.sharepoint.online;

import com.github.quantrresearch.sharepoint.online.datastructure.ServerInfo;
import com.github.quantrresearch.sharepoint.online.jgraph.MyGraphComponent;
import com.github.quantrresearch.sharepoint.online.jgraph.MyJGraphCanvas;
import com.google.common.collect.Lists;
import com.google.common.net.UrlEscapers;
import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.orthogonal.mxOrthogonalLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxCompactTreeLayout;
import com.mxgraph.layout.mxEdgeLabelLayout;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.layout.mxOrganicLayout;
import com.mxgraph.layout.mxParallelEdgeLayout;
import com.mxgraph.layout.mxStackLayout;
import hk.quantr.javalib.CommonLib;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.model.ExternalLinksTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;
import hk.quantr.sharepoint.SPOnline;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.JSONObject;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
		dtd = "-//com.github.quantrresearch.sharepoint.online//ExcelTransform//EN",
		autostore = false
)
@TopComponent.Description(
		preferredID = "ExcelTransformTopComponent",
		iconBase = "com/github/quantrresearch/sharepoint/online/fileicon/excel.png",
		persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "output", openAtStartup = false)
@ActionID(category = "Window", id = "com.github.quantrresearch.sharepoint.online.ExcelTransformTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
		displayName = "#CTL_ExcelTransformAction",
		preferredID = "ExcelTransformTopComponent"
)
@Messages({
	"CTL_ExcelTransformAction=ExcelTransform",
	"CTL_ExcelTransformTopComponent=ExcelTransform Window",
	"HINT_ExcelTransformTopComponent=This is a ExcelTransform window"
})
public final class ExcelTransformTopComponent extends TopComponent {

	static int vertexWidth = 150;
	static int vertexHeight = 50;

	mxGraph graph = new mxGraph() {

		@Override
		public boolean isCellMovable(Object cell) {
			return false;
		}

		@Override
		public boolean isCellEditable(Object cell) {
			return false;
		}

		@Override
		public String getToolTipForCell(Object cell) {
			if (model.isEdge(cell)) {
				return convertValueToString(model.getTerminal(cell, true)) + " -> " + convertValueToString(model.getTerminal(cell, false));
			}

			return super.getToolTipForCell(cell);
		}

		@Override
		public boolean isCellFoldable(Object cell, boolean collapse) {
			return false;
		}

		@Override
		public void drawState(mxICanvas canvas, mxCellState state, boolean drawLabel) {
			mxCell mxCell = (mxCell) state.getCell();
			if (mxCell != null) {
				String str = (String) mxCell.getValue();
				if (getModel().isVertex(state.getCell()) && canvas instanceof MyJGraphCanvas) {
					((MyJGraphCanvas) canvas).drawVertex(state, str);
				} else {
					super.drawState(canvas, state, drawLabel);
				}
			}
		}
	};
	MyGraphComponent graphComponent = new MyGraphComponent(graph);
	JFileChooser fileChooser = new JFileChooser();
	HashMap<String, ArrayList<String>> linkages = new HashMap<String, ArrayList<String>>();
	HashMap<String, mxCell> addedNode = new HashMap<String, mxCell>();
	ScanningTableModel scanningTableModel = new ScanningTableModel();

	public ExcelTransformTopComponent() {
		initComponents();
		setName(Bundle.CTL_ExcelTransformTopComponent());
		setToolTipText(Bundle.HINT_ExcelTransformTopComponent());

		layoutButton.removeAll();
		layoutButton.add(new JMenuItem("Compact Tree Layout"));
		layoutButton.add(new JMenuItem("Hierarchical Layout"));
		layoutButton.add(new JMenuItem("Circle Layout"));
		layoutButton.add(new JMenuItem("Organic Layout"));
		layoutButton.add(new JMenuItem("Edge Label Layout"));
		layoutButton.add(new JMenuItem("Fast Organic Layout"));
		layoutButton.add(new JMenuItem("Orthogonal Layout"));
		layoutButton.add(new JMenuItem("Parallel Edge Layout"));
		layoutButton.add(new JMenuItem("Stack Layout"));

		graphComponent.setConnectable(false);
//		graphComponent.setOpaque(true);
//		graphComponent.setBackground(Color.white);
		graphComponent.getViewport().setOpaque(true);
		graphComponent.getViewport().setBackground(Color.white);

		graphScrollPane.getViewport().add(graphComponent);
		scanningTable.setModel(scanningTableModel);
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
	 * Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        openButton = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        relationshipPanel = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        refreshGraphvizButton = new javax.swing.JButton();
        layoutButton = new hk.quantr.javalib.swing.advancedswing.jdropdownbutton.JDropDownButton();
        graphScrollPane = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        scanningTable = new javax.swing.JTable();
        jToolBar2 = new javax.swing.JToolBar();
        scanButton = new javax.swing.JButton();
        processButton = new javax.swing.JButton();
        exportExcelButton = new javax.swing.JButton();
        uploadPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        uploadTable = new javax.swing.JTable();
        jToolBar3 = new javax.swing.JToolBar();
        uploadButton = new javax.swing.JButton();
        reloadButton = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        openScanOnlyButton = new javax.swing.JButton();

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/github/quantrresearch/sharepoint/online/icon/folder.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(openButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.openButton.text_1")); // NOI18N
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });

        relationshipPanel.setLayout(new java.awt.BorderLayout());

        jToolBar1.setRollover(true);

        refreshGraphvizButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/github/quantrresearch/sharepoint/online/icon/arrow_refresh.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(refreshGraphvizButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.refreshGraphvizButton.text_1")); // NOI18N
        refreshGraphvizButton.setFocusable(false);
        refreshGraphvizButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        refreshGraphvizButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshGraphvizButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshGraphvizButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(refreshGraphvizButton);

        org.openide.awt.Mnemonics.setLocalizedText(layoutButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.layoutButton.text")); // NOI18N
        layoutButton.setFocusable(false);
        layoutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        layoutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(layoutButton);

        relationshipPanel.add(jToolBar1, java.awt.BorderLayout.PAGE_START);
        relationshipPanel.add(graphScrollPane, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab(org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.relationshipPanel.TabConstraints.tabTitle_1"), relationshipPanel); // NOI18N

        jPanel1.setLayout(new java.awt.BorderLayout());

        scanningTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scanningTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        scanningTable.setRowHeight(26);
        jScrollPane1.setViewportView(scanningTable);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jToolBar2.setRollover(true);

        scanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/github/quantrresearch/sharepoint/online/icon/magnifier.png"))); // NOI18N
        scanButton.setFocusable(false);
        scanButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        scanButton.setLabel(org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.scanButton.label_1")); // NOI18N
        scanButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        scanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scanButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(scanButton);

        processButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/github/quantrresearch/sharepoint/online/icon/disk.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(processButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.processButton.text_1")); // NOI18N
        processButton.setFocusable(false);
        processButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        processButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        processButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(processButton);

        exportExcelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/github/quantrresearch/sharepoint/online/fileicon/excel.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(exportExcelButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.exportExcelButton.text")); // NOI18N
        exportExcelButton.setFocusable(false);
        exportExcelButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        exportExcelButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        exportExcelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportExcelButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(exportExcelButton);

        jPanel1.add(jToolBar2, java.awt.BorderLayout.PAGE_START);

        jTabbedPane1.addTab(org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.jPanel1.TabConstraints.tabTitle_1"), jPanel1); // NOI18N

        uploadPanel.setLayout(new java.awt.BorderLayout());

        uploadTable.setModel(new UploadTableModel());
        jScrollPane2.setViewportView(uploadTable);

        uploadPanel.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jToolBar3.setRollover(true);

        org.openide.awt.Mnemonics.setLocalizedText(uploadButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.uploadButton.text")); // NOI18N
        uploadButton.setFocusable(false);
        uploadButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        uploadButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        uploadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(uploadButton);

        uploadPanel.add(jToolBar3, java.awt.BorderLayout.PAGE_START);

        jTabbedPane1.addTab(org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.uploadPanel.TabConstraints.tabTitle"), uploadPanel); // NOI18N

        reloadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/github/quantrresearch/sharepoint/online/icon/arrow_refresh.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(reloadButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.reloadButton.text_1")); // NOI18N
        reloadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadButtonActionPerformed(evt);
            }
        });

        progressBar.setStringPainted(true);

        openScanOnlyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/github/quantrresearch/sharepoint/online/icon/folder.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(openScanOnlyButton, org.openide.util.NbBundle.getMessage(ExcelTransformTopComponent.class, "ExcelTransformTopComponent.openScanOnlyButton.text")); // NOI18N
        openScanOnlyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openScanOnlyButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1184, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(openButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(openScanOnlyButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(reloadButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(openButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(reloadButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(openScanOnlyButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 868, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int option = fileChooser.showOpenDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) {
			reload(false, true);
		}
    }//GEN-LAST:event_openButtonActionPerformed

    private void refreshGraphvizButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshGraphvizButtonActionPerformed
		buildJGraph();
		layoutButtonActionPerformed(null);
    }//GEN-LAST:event_refreshGraphvizButtonActionPerformed

    private void layoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_layoutButtonActionPerformed
		final mxGraph graph = graphComponent.getGraph();
		Object cell = graph.getSelectionCell();

		if (cell == null || graph.getModel().getChildCount(cell) == 0) {
			cell = graph.getDefaultParent();
		}
		graph.getModel().beginUpdate();

		String str;
		if (layoutButton.getEventSource() == null) {
			str = "Hierarchical Layout";
		} else {
			str = ((JMenuItem) layoutButton.getEventSource()).getText();
		}
		if (str.equals("Hierarchical Layout")) {
			mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Circle Layout")) {
			mxCircleLayout layout = new mxCircleLayout(graph);
			layout.setDisableEdgeStyle(false);
			layout.execute(cell);
		} else if (str.equals("Organic Layout")) {
			mxOrganicLayout layout = new mxOrganicLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Compact Tree Layout")) {
			mxCompactTreeLayout layout = new mxCompactTreeLayout(graph, false);
			layout.execute(cell);
		} else if (str.equals("Edge Label Layout")) {
			mxEdgeLabelLayout layout = new mxEdgeLabelLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Fast Organic Layout")) {
			mxFastOrganicLayout layout = new mxFastOrganicLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Orthogonal Layout")) {
			mxOrthogonalLayout layout = new mxOrthogonalLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Parallel Edge Layout")) {
			mxParallelEdgeLayout layout = new mxParallelEdgeLayout(graph);
			layout.execute(cell);
		} else if (str.equals("Stack Layout")) {
			mxStackLayout layout = new mxStackLayout(graph);
			layout.execute(cell);
		} else {
			System.out.println("no this layout");
		}

		mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);
		morph.addListener(mxEvent.DONE, new mxEventSource.mxIEventListener() {
			public void invoke(Object sender, mxEventObject evt) {
				graph.getModel().endUpdate();
			}
		});

		morph.startAnimation();
    }//GEN-LAST:event_layoutButtonActionPerformed

    private void scanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scanButtonActionPerformed
		reload(false, true);
    }//GEN-LAST:event_scanButtonActionPerformed

    private void processButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processButtonActionPerformed
		reload(true, true);
    }//GEN-LAST:event_processButtonActionPerformed

    private void reloadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadButtonActionPerformed
		reload(false, true);
    }//GEN-LAST:event_reloadButtonActionPerformed

    private void uploadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadButtonActionPerformed
		ServerInfo serverInfo = (ServerInfo) Helper.getNodeObject(SharePointTopComponent.node, ServerInfo.class);
		try {
			Pair<String, String> token = SPOnline.login(serverInfo.username, serverInfo.password, serverInfo.domain);
			if (token != null) {
				String jsonString = SPOnline.post(token, serverInfo.domain, "/_api/contextinfo", null, null);
				JSONObject json = new JSONObject(jsonString);
				String formDigestValue = json.getJSONObject("d").getJSONObject("GetContextWebInformation").getString("FormDigestValue");

				progressBar.setValue(0);
				progressBar.setString("Starting");
				File dir = fileChooser.getSelectedFile();
				if (dir != null) {
					ArrayList<File> files = new ArrayList<File>();
					listFilesAndDirectory(dir, files, progressBar);
					progressBar.setMaximum(files.size());
					String basePath = dir.getAbsolutePath();
					for (File file : files) {
						String path = file.getAbsolutePath().substring(basePath.length());
						path = path.substring(0, path.length() - file.getName().length());
						path = path.replaceAll("\\\\", "/");
						if (file.isFile()) {
							jsonString = SPOnline.post(token, serverInfo.domain, "/" + serverInfo.path + "/_api/web/getfolderbyserverrelativeurl('" + UrlEscapers.urlFragmentEscaper().escape(SharePointTopComponent.node.text) + "/" + UrlEscapers.urlFragmentEscaper().escape(path) + "')/files/add(overwrite=true,url='" + UrlEscapers.urlFragmentEscaper().escape(file.getName()) + "')", file, formDigestValue, true);
							if (jsonString != null) {
								System.out.println(CommonLib.prettyFormatJson(jsonString));
							}
						} else if (file.isDirectory()) {
							jsonString = SPOnline.post(token, serverInfo.domain, "/" + serverInfo.path + "/_api/web/getfolderbyserverrelativeurl('" + UrlEscapers.urlFragmentEscaper().escape(SharePointTopComponent.node.text) + "/" + UrlEscapers.urlFragmentEscaper().escape(path) + "')/folders/add(url='" + UrlEscapers.urlFragmentEscaper().escape(file.getName()) + "')", null, formDigestValue);
							if (jsonString != null) {
								System.out.println(CommonLib.prettyFormatJson(jsonString));
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }//GEN-LAST:event_uploadButtonActionPerformed

    private void exportExcelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportExcelButtonActionPerformed
		XSSFWorkbook file_uploaded_workbook = new XSSFWorkbook();
		XSSFSheet file_uploaded_worksheet1 = file_uploaded_workbook.createSheet("Worksheet1");
		Row row = file_uploaded_worksheet1.createRow(0);
		for (int x = 0; x < scanningTableModel.getColumnCount(); x++) {
			row.createCell(x).setCellValue(scanningTableModel.getColumnName(x));
		}

		for (int y = 0; y < scanningTableModel.getRowCount(); y++) {
			row = file_uploaded_worksheet1.createRow(y + 1);
			for (int x = 0; x < scanningTableModel.getColumnCount(); x++) {
				row.createCell(x).setCellValue(scanningTableModel.getValueAt(y, x).toString());
			}
		}
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try (FileOutputStream outputStream = new FileOutputStream(fc.getSelectedFile())) {
				file_uploaded_workbook.write(outputStream);
			} catch (FileNotFoundException ex) {
				Exceptions.printStackTrace(ex);
			} catch (IOException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
    }//GEN-LAST:event_exportExcelButtonActionPerformed

    private void openScanOnlyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openScanOnlyButtonActionPerformed
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int option = fileChooser.showOpenDialog(this);
		if (option == JFileChooser.APPROVE_OPTION) {
			reload(false, false);
		}
    }//GEN-LAST:event_openScanOnlyButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton exportExcelButton;
    private javax.swing.JScrollPane graphScrollPane;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private hk.quantr.javalib.swing.advancedswing.jdropdownbutton.JDropDownButton layoutButton;
    private javax.swing.JButton openButton;
    private javax.swing.JButton openScanOnlyButton;
    private javax.swing.JButton processButton;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JButton refreshGraphvizButton;
    private javax.swing.JPanel relationshipPanel;
    private javax.swing.JButton reloadButton;
    private javax.swing.JButton scanButton;
    private javax.swing.JTable scanningTable;
    private javax.swing.JButton uploadButton;
    private javax.swing.JPanel uploadPanel;
    private javax.swing.JTable uploadTable;
    // End of variables declaration//GEN-END:variables
	@Override
	public void componentOpened() {
	}

	@Override
	public void componentClosed() {
	}

	void writeProperties(java.util.Properties p) {
		p.setProperty("version", "1.0");
	}

	void readProperties(java.util.Properties p) {
		String version = p.getProperty("version");
	}

	private void buildJGraph() {
		graph.removeCells(graph.getChildVertices(graph.getDefaultParent()));
		addedNode.clear();

		for (String fromExcel : linkages.keySet()) {
			mxCell newNode;
			newNode = addedNode.get(fromExcel);
			if (newNode == null) {
				newNode = (mxCell) graph.insertVertex(null, null, fromExcel, 40, 40, 150, 30);
				mxGeometry geometry = newNode.getGeometry();
				geometry.setWidth(vertexWidth);
				geometry.setHeight(vertexHeight);
				addedNode.put(fromExcel, newNode);
			}
			for (String toExcel : linkages.get(fromExcel)) {
				buildJGraphSubNode(newNode, toExcel);
			}
		}

		graph.setCellsDisconnectable(false);
		graph.getModel().beginUpdate();
		mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);
		morph.addListener(mxEvent.DONE, new mxEventSource.mxIEventListener() {
			public void invoke(Object sender, mxEventObject evt) {
				graph.getModel().endUpdate();
			}
		});

		morph.startAnimation();
	}

	private void buildJGraphSubNode(mxCell fromNode, String toExcel) {
		mxCell newNode;
		newNode = addedNode.get(toExcel);
		if (newNode == null) {
			newNode = (mxCell) graph.insertVertex(null, null, toExcel, 40, 40, 150, 30);
			mxGeometry geometry = newNode.getGeometry();
			geometry.setWidth(vertexWidth);
			geometry.setHeight(vertexHeight);
			addedNode.put(toExcel, newNode);
		}
		graph.insertEdge(null, null, "", fromNode, newNode, mxConstants.STYLE_STROKECOLOR + "=#000000;edgeStyle=elbowEdgeStyle;");
	}

	public void listFiles(File directory, ArrayList<File> files, JProgressBar progressBar) {
		System.out.println("Scanning folder : " + directory.getAbsolutePath());
		progressBar.setString("Scanning folder : " + directory.getAbsolutePath());
		try {
			File[] fList = directory.listFiles();
			for (File file : fList) {
				if (file.isFile()) {
					files.add(file);
				} else if (file.isDirectory()) {
					listFiles(file, files, progressBar);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void listFilesAndDirectory(File directory, ArrayList<File> files, JProgressBar progressBar) {
		System.out.println("Scanning folder : " + directory.getAbsolutePath());
		progressBar.setString("Scanning folder : " + directory.getAbsolutePath());
		try {
			File[] fList = directory.listFiles();
			for (File file : fList) {
				files.add(file);
				if (file.isDirectory()) {
					listFilesAndDirectory(file, files, progressBar);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static Set<String> getReferencedWorkbooks(XSSFWorkbook workbook) {
		Set<String> workbookNames = new HashSet<>();
		final List<ExternalLinksTable> externalLinksTable = workbook.getExternalLinksTable();
		for (ExternalLinksTable linksTable : externalLinksTable) {
			final String linkedFileName = linksTable.getLinkedFileName();
			workbookNames.add(linkedFileName);
		}

		return workbookNames;
	}

	private void reload(boolean isProcess, boolean realScan) {
//		if (SharePointTopComponent.serverInfo == null) {
//			JOptionPane.showMessageDialog(null, "Please login to SharePoint first", "Error", JOptionPane.ERROR_MESSAGE);
//			return;
//		}
//		if (SharePointTopComponent.node == null) {
//			JOptionPane.showMessageDialog(null, "Please select a document library in the SharePoint tree panel", "Error", JOptionPane.ERROR_MESSAGE);
//			return;
//		}
//		if (!SharePointTopComponent.node.type.equals("docLib")) {
//			JOptionPane.showMessageDialog(null, "You have selected a wrong thing, please select a document library in the SharePoint tree panel", "Error", JOptionPane.ERROR_MESSAGE);
//			return;
//		}

		try {
			File tempFile = new File("scan.txt");
			FileWriter fr = new FileWriter(tempFile, true);
			BufferedWriter br = new BufferedWriter(fr);
			PrintWriter pr = new PrintWriter(br);

			progressBar.setValue(0);
			progressBar.setString("Starting");
			File dir = fileChooser.getSelectedFile();
			if (dir != null) {
				scanningTableModel.files.clear();
				scanningTableModel.formulas.clear();
				scanningTableModel.replacements.clear();

				ArrayList<File> files = new ArrayList<File>();
				listFiles(dir, files, progressBar);
				progressBar.setMaximum(files.size());
				for (File file : files) {
					if (!file.getName().endsWith(".xls") && !file.getName().endsWith(".xlsx")) {
						continue;
					}
					progressBar.setValue(progressBar.getValue() + 1);
					progressBar.setString("Processing " + file.getAbsolutePath());
					System.out.println("Processing " + file.getAbsolutePath());
					try {
						pr.println(file.getAbsolutePath());
						FileInputStream fis = new FileInputStream(file);

						ArrayList<String> linkedExcelList = linkages.get(file.getName());
						if (linkedExcelList == null) {
							linkedExcelList = new ArrayList<String>();
							linkages.put(file.getName(), linkedExcelList);
						}

//					Workbook wb = new XSSFWorkbook(fis);
						Workbook wb = new XSSFWorkbook(fis);
						wb.setForceFormulaRecalculation(false);
						Set<String> set = getReferencedWorkbooks((XSSFWorkbook) wb);
						List<String> externalNames = Lists.newArrayList(set);
//						ModuleLib.log("ss=" + set.size());
						for (String s : set) {
							linkedExcelList.add(s);
							pr.println("  linked to > " + s);
						}

						/*FormulaEvaluator mainWorkbookEvaluator = wb.getCreationHelper().createFormulaEvaluator();
						Map<String, FormulaEvaluator> workbooks = new HashMap<String, FormulaEvaluator>();
						workbooks.put(file.getName(), mainWorkbookEvaluator);
						for (File file2 : files) {
							if (file == file2) {
								continue;
							}
//							ModuleLib.log(">>" + file2.getName());
							workbooks.put(file2.getName(), WorkbookFactory.create(file2).getCreationHelper().createFormulaEvaluator());
						}
						mainWorkbookEvaluator.setupReferencedWorkbooks(workbooks);
						mainWorkbookEvaluator.evaluateAll();*/
						Sheet sheet = wb.getSheetAt(0);
						int maxRow = sheet.getLastRowNum();
						for (int y = 0; y <= maxRow; y++) {
							Row row = sheet.getRow(y);
							for (int x = 0; x <= row.getLastCellNum(); x++) {
								Cell cell = row.getCell(x);
								if (cell != null) {
									if (cell.getCellTypeEnum() == CellType.FORMULA) {
										Row newRow;
										newRow = sheet.getRow(y);
										if (newRow == null) {
											newRow = sheet.createRow(y);
										}
										Cell newCell;
										newCell = newRow.getCell(x);
										if (newCell == null) {
											newCell = newRow.createCell(x);
										}
										//newCell.setCellFormula(cell.getCellFormula());
										String link = cell.getCellFormula();
										if (realScan) {
											if (!link.contains(SharePointTopComponent.serverInfo.domain + ".sharepoint.com")) {
												scanningTableModel.files.add(file.getName());

												scanningTableModel.formulas.add(link);
												for (int z = 0; z < externalNames.size(); z++) {
													ModuleLib.log("+" + "'https://" + SharePointTopComponent.serverInfo.domain + ".sharepoint.com/" + SharePointTopComponent.node.text + "/[" + externalNames.get(z) + "]");
													pr.println("  + " + "'https://" + SharePointTopComponent.serverInfo.domain + ".sharepoint.com/" + SharePointTopComponent.node.text + "/[" + externalNames.get(z) + "]");
													link = link.replaceAll("\\[" + (z + 1) + "\\]", "'https://" + SharePointTopComponent.serverInfo.domain + ".sharepoint.com/" + SharePointTopComponent.node.text + "/[" + externalNames.get(z) + "]");
												}
												link = link.replaceAll("!", "'!");
												String newFormula = link;
												newCell.setCellFormula(newFormula);
												scanningTableModel.replacements.add(newFormula);
												ModuleLib.log(file.getName() + " - " + x + "," + y + " = " + cell.getCellFormula());
												pr.println("  - " + file.getName() + " - " + x + "," + y + " = " + cell.getCellFormula());
											}
										} else {
											//										if (!link.contains(SharePointTopComponent.serverInfo.domain + ".sharepoint.com")) {
											scanningTableModel.files.add(file.getName());

											scanningTableModel.formulas.add(link);
											for (int z = 0; z < externalNames.size(); z++) {
												ModuleLib.log("+" + "'https://<tenant>.sharepoint.com/<site>/[" + externalNames.get(z) + "]");
												pr.println("  + " + "'https://<tenant>.sharepoint.com/<site>/[" + externalNames.get(z) + "]");
												link = link.replaceAll("\\[" + (z + 1) + "\\]", "'https://<tenant>.sharepoint.com/<site>/[" + externalNames.get(z) + "]");
											}
											link = link.replaceAll("!", "'!");
											String newFormula = link;
											newCell.setCellFormula(newFormula);
											scanningTableModel.replacements.add(newFormula);
											ModuleLib.log(file.getName() + " - " + x + "," + y + " = " + cell.getCellFormula());
											pr.println("  - " + file.getName() + " - " + x + "," + y + " = " + cell.getCellFormula());
//										}
										}
									}
								}
							}
						}

						if (isProcess) {
							FileOutputStream outFile = new FileOutputStream(file);
							wb.write(outFile);
							outFile.close();
						}
						progressBar.setString("Finished");
					} catch (FileNotFoundException ex) {
						Exceptions.printStackTrace(ex);
						progressBar.setString("Error " + ex.getMessage());
					} catch (IOException ex) {
						Exceptions.printStackTrace(ex);
						progressBar.setString("Error " + ex.getMessage());
					} catch (EncryptedDocumentException ex) {
						Exceptions.printStackTrace(ex);
						progressBar.setString("Error " + ex.getMessage());
					} catch (Exception ex) {
						Exceptions.printStackTrace(ex);
						progressBar.setString("Error " + ex.getMessage());
					}
				}
				refreshGraphvizButtonActionPerformed(null);
				scanningTableModel.fireTableDataChanged();
				CommonLib.autoResizeColumn(scanningTable);
			}
			pr.close();
			br.close();
			fr.close();
			JOptionPane.showMessageDialog(null, "Finish");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
